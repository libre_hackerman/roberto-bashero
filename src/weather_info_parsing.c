/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <weather_info_parsing.h>

/*
** This function looks for the "periodo" attribute in the specified node and
** returns its value as an integer. Returns -1 if the attribute couldn't be
** found or the value was outside the [0, 23] interval.
*/

static int
get_period_hour(xmlNode *node)
{
	xmlAttr *cur_attr;
	int hour;

	for (cur_attr = node->properties; cur_attr; cur_attr = cur_attr->next)
	{
		if (strcmp((char*)cur_attr->name, "periodo") == 0)
		{
			hour = atoi((char*)cur_attr->children->content);
			return (hour >= 0 && hour <= 23 ? hour : -1);
		}
	}
	return (-1);
}

/*
** This function, receiving the hours array and a sky_state node, will
** store in the right hour the sky_state.
*/

void
parse_sky_state(struct period_s *hours, xmlNode *node)
{
	int hour;

	if ((hour = get_period_hour(node)) == -1)
	{
		fprintf(stderr, "Can't get period hour for sky_state\n");
		return;
	}

	if (node->children)
	{
		hours[hour].initialized = 1;
		hours[hour].sky_state = atoi((char*)node->children->content);
	}
}

/*
** This function, receiving the hours array and a precipitation node, will
** store in the right hour the precipitation.
*/

void
parse_precipitation(struct period_s *hours, xmlNode *node)
{
	int hour;

	if ((hour = get_period_hour(node)) == -1)
	{
		fprintf(stderr, "Can't get period hour for precipitation\n");
		return;
	}

	if (node->children)
	{
		hours[hour].initialized = 1;
		hours[hour].precipitation = strdup((char*)node->children->content);
	}
}

/*
** This function, receiving the hours array and a snow node, will
** store in the right hour the snow.
*/

void
parse_snow(struct period_s *hours, xmlNode *node)
{
	int hour;

	if ((hour = get_period_hour(node)) == -1)
	{
		fprintf(stderr, "Can't get period hour for snow\n");
		return;
	}

	if (node->children)
	{
		hours[hour].initialized = 1;
		hours[hour].snow = strdup((char*)node->children->content);
	}
}

/*
** This function, receiving the hours array and a temperature node, will
** store in the right hour the temperature.
*/

void
parse_temperature(struct period_s *hours, xmlNode *node)
{
	int hour;

	if ((hour = get_period_hour(node)) == -1)
	{
		fprintf(stderr, "Can't get period hour for temperature\n");
		return;
	}

	if (node->children)
	{
		hours[hour].initialized = 1;
		hours[hour].temperature = strdup((char*)node->children->content);
	}
}

/*
** This function, receiving the hours array and a termic_sensation node, will
** store in the right hour the termic_sensation.
*/

void
parse_termic_sensation(struct period_s *hours, xmlNode *node)
{
	int hour;

	if ((hour = get_period_hour(node)) == -1)
	{
		fprintf(stderr, "Can't get period hour for termic_sensation\n");
		return;
	}

	if (node->children)
	{
		hours[hour].initialized = 1;
		hours[hour].termic_sensation = strdup((char*)node->children->content);
	}
}

/*
** This function, receiving the hours array and a rel_humidity node, will
** store in the right hour the rel_humidity.
*/

void
parse_rel_humidity(struct period_s *hours, xmlNode *node)
{
	int hour;

	if ((hour = get_period_hour(node)) == -1)
	{
		fprintf(stderr, "Can't get period hour for rel_humidity\n");
		return;
	}

	if (node->children)
	{
		hours[hour].initialized = 1;
		hours[hour].rel_humidity = strdup((char*)node->children->content);
	}
}

/*
** This function, receiving the hours array and a wind node, will
** store in the right hour the wind direction and the wind speed.
*/

void
parse_wind(struct period_s *hours, xmlNode *node)
{
	int hour;
	xmlNode *cur;

	if ((hour = get_period_hour(node)) == -1)
	{
		fprintf(stderr, "Can't get period hour for wind\n");
		return;
	}

	for (cur = node->children; cur; cur = cur->next)
	{
		/* Check if the node is an XML element */
		if (cur->type != XML_ELEMENT_NODE)
			continue;

		if (strcmp((char*)cur->name, "direccion") == 0)
		{
			hours[hour].initialized = 1;
			hours[hour].wind_direction = strdup((char*)cur->children->content);
		}
		else if (strcmp((char*)cur->name, "velocidad") == 0)
		{
			hours[hour].initialized = 1;
			hours[hour].wind_speed = strdup((char*)cur->children->content);
		}
	}
}

/*
** This function, receiving the hours array and a max_wind_gust node, will
** store in the right hour the max_wind_gust.
*/

void
parse_max_wind_gust(struct period_s *hours, xmlNode *node)
{
	int hour;

	if ((hour = get_period_hour(node)) == -1)
	{
		fprintf(stderr, "Can't get period hour for max_wind_gust\n");
		return;
	}

	if (node->children)
	{
		hours[hour].initialized = 1;
		hours[hour].max_wind_gust = strdup((char*)node->children->content);
	}
}
