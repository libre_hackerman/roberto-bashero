/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PREDICTION_PARSER_H
#define PREDICTION_PARSER_H

struct period_s
{
	int initialized;
	int sky_state;
	char *precipitation;
	char *snow;
	char *temperature;
	char *termic_sensation;
	char *rel_humidity;
	char *wind_direction;
	char *wind_speed;
	char *max_wind_gust;
};

struct day_s
{
	char *data;
	struct period_s hours[24];
};

struct prediction_s
{
	char *city;
	char *province;
	char *updated_data;
	int n_days;
	struct day_s *days;
};

struct prediction_s*
get_prediction(char *city_code);

void
delete_prediction(struct prediction_s *pred);

#endif
