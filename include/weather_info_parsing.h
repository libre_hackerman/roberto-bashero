/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WEATHER_INFO_PARSING_H
#define WEATHER_INFO_PARSING_H

#include <libxml/tree.h>
#include <prediction_parser.h>

void
parse_sky_state(struct period_s *hours, xmlNode *node);

void
parse_precipitation(struct period_s *hours, xmlNode *node);

void
parse_snow(struct period_s *hours, xmlNode *node);

void
parse_temperature(struct period_s *hours, xmlNode *node);

void
parse_termic_sensation(struct period_s *hours, xmlNode *node);

void
parse_rel_humidity(struct period_s *hours, xmlNode *node);

void
parse_wind(struct period_s *hours, xmlNode *node);

void
parse_max_wind_gust(struct period_s *hours, xmlNode *node);

#endif
