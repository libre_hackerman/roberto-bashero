![roberto-bashero example](screenshot.png)

# Roberto-Bashero

`roberto-bashero` is a CLI utility to quickly display the weather forecast from [AEMET](http://www.aemet.es) on an hourly basis with a configurable interface.

### CLI arguments
- `--city-code`: to set for which municipality get the forecast.
- `--days`: to set how many days you want to see in the forecast.
- `--fields`: if you don't want to see all the meteorological variables, you can filter which ones display with this option.
- `--no-pager`: By default it will display the output using a PAGER if the output is too long to fit in the terminal, use this option to disable that.
- `--help`: display a short guide about the use of the program.

### City codes

You need to specify a particular five digits code to get the forecast of a particular municipality. To get it, go to [the AEMET page](http://www.aemet.es/es/eltiempo/prediccion/municipios) and open the forecast for the city you want. The code is at the end of the URL.

For example, in the case of Barcelona, the URL is `http://www.aemet.es/es/eltiempo/prediccion/municipios/barcelona-id08019` and the code would be `08019`.

### Build

#### Dependencies
- Make
- C compiler
- libxml2

#### Compilation
Just run `make`. If you want debug symbols, run `make debug`

#### Installation
You can directly run the compiled executable, but if you want to install it on your system, `make install` will do it. Notice that you can set the variable `PREFIX` (default to /usr/local) to your desire

#### Uninstall
`make uninstall` (Mind the `PREFIX` too)

### FAQ
- **This is a Spain specific program. Why is everything in English?**
Esa es una buena pregunta, amigo hispanohablante. En un principio solo el código iba a estar en inglés, por coherencia con el propio lenguaje de programación. Pero sin darme cuenta fui haciendo cada vez más cosas en inglés hasta que llegó un punto en que decidí hacerlo todo en ese idioma. No obstante, si alguien está interesado en una versión con la interfaz en castellano, no me importaría en principio traducirlo.
