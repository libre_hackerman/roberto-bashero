/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CHARACTER_CODES_H
#define CHARACTER_CODES_H

/* FOREGROUND COLORS */
#define FOR_BLACK "\033[30m"
#define FOR_RED "\033[31m"
#define FOR_GREEN "\033[32m"
#define FOR_YELLOW "\033[33m"
#define FOR_BLUE "\033[34m"
#define FOR_PURPLE "\033[35m"
#define FOR_CYAN "\033[36m"
#define FOR_WHITE "\033[37m"

/* BACKGROUND COLORS */
#define BACK_BLACK "\033[40m"
#define BACK_RED "\033[41m"
#define BACK_GREEN "\033[42m"
#define BACK_YELLOW "\033[43m"
#define BACK_BLUE "\033[44m"
#define BACK_PURPLE "\033[45m"
#define BACK_CYAN "\033[46m"
#define BACK_WHITE "\033[47m"

/* OTHER STYLE */
#define RESET "\033[0m"
#define BOLD "\033[1m"
#define UNDERLINE "\033[4m"
#define INVERSE "\033[7m"
#define OFF_BOLD "\033[21m"
#define OFF_UNDERLINE "\033[24m"
#define OFF_INVERSE "\033[27m"

/* BOX */
#define BOX_HORIZONTAL L'\x2500'
#define BOX_VERTICAL L'\x2502'
#define BOX_CROSS L'\x253C'

/* DOUBLE BOX */
#define DBOX_HORIZONTAL L'\x2550'
#define DBOX_VERTICAL L'\x2551'
#define DBOX_UPPER_LEFT_CORNER L'\x2554'
#define DBOX_UPPER_RIGHT_CORNER L'\x2557'
#define DBOX_DOWN_LEFT_CORNER L'\x255A'
#define DBOX_DOWN_RIGHT_CORNER L'\x255D'
#define DBOX_VERTICAL_RIGHT L'\x2560'
#define DBOX_VERTICAL_LEFT L'\x2563'
#define DBOX_HORIZONTAL_DOWN L'\x2566'
#define DBOX_CROSS L'\x256C'
#define DBOX_VERTICAL_S_RIGHT L'\x255F'
#define DBOX_VERTICAL_S_LEFT L'\x2562'
#define DBOX_HORIZONTAL_S_UP L'\x2567'

/* Arrows */
#define LEFT_ARROW L'\x2190'
#define UP_ARROW L'\x2191'
#define RIGHT_ARROW L'\x2192'
#define DOWN_ARROW L'\x2193'
#define UP_LEFT_ARROW L'\x2196'
#define UP_RIGHT_ARROW L'\x2197'
#define DOWN_RIGHT_ARROW L'\x2198'
#define DOWN_LEFT_ARROW L'\x2199'

#endif
