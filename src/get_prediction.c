/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <libxml/parser.h>
#include <libxml/tree.h>
#include <curl/curl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <prediction_parser.h>
#include <weather_info_parsing.h>

static void
parse_day_prediction(struct period_s *hours, xmlNode *weather_info)
{
	for (xmlNode *cur = weather_info; cur; cur = cur->next)
	{
		/* Check if the node is an XML element */
		if (cur->type != XML_ELEMENT_NODE)
			continue;

		/* Check what type of weather info this node contains */
		if (strcmp((char*)cur->name, "estado_cielo") == 0)
			parse_sky_state(hours, cur);
		else if (strcmp((char*)cur->name, "precipitacion") == 0)
			parse_precipitation(hours, cur);
		else if (strcmp((char*)cur->name, "nieve") == 0)
			parse_snow(hours, cur);
		else if (strcmp((char*)cur->name, "temperatura") == 0)
			parse_temperature(hours, cur);
		else if (strcmp((char*)cur->name, "sens_termica") == 0)
			parse_termic_sensation(hours, cur);
		else if (strcmp((char*)cur->name, "humedad_relativa") == 0)
			parse_rel_humidity(hours, cur);
		else if (strcmp((char*)cur->name, "viento") == 0)
			parse_wind(hours, cur);
		else if (strcmp((char*)cur->name, "racha_max") == 0)
			parse_max_wind_gust(hours, cur);
	}
}

/*
** This function allocates the days array (and the hours array inside each
** day) given the number of days.
*/

static struct day_s*
allocate_days(int n_days)
{
	struct day_s *days;

	/* Array of days */
	if (!(days = calloc(sizeof(struct day_s), n_days + 1)))
	{
		perror("Can't allocate struct day_s array");
		return (NULL);
	}

	/* Array of hours */
	for (int i = 0; i < n_days; i++)
		days[i].data = NULL;

	return (days);
}

/*
** This function reads the data from raw_data and stores it
** (with "hour:minute:second - day/month/year" format) into pred->updated_data.
*/

static void
parse_updated_data(struct prediction_s *pred, const char *raw_data)
{
	char sec[3], min[3], hour[3], day[3], month[3], year[5];
	char *updated_data;

	/* Allocate target string */
	if (!(updated_data = malloc(22)))
	{
		perror("Can't allocate updated_data");
		return;
	}

	/* Parse data fields */
	sscanf(raw_data, "%4s-%2s-%2sT%2s:%2s:%2s", year, month, day, hour,
			min, sec);

	/* Write fields to target string */
	sprintf(updated_data, "%s:%s:%s - %s/%s/%s", hour, min, sec, day,
			month, year);

	pred->updated_data = updated_data;
}

/*
** This function will store in pred->prediction the data from the
** days contained in the XML, given the node to the first of the days.
*/

static void
parse_prediction(struct prediction_s *pred, xmlNode *days)
{
	int n_days, day_index;
	xmlAttr *day_attr;

	/* Count the days stored as siblings of days */
	n_days = 0;
	for (xmlNode *day = days; day; day = day->next)
		if (day->type == XML_ELEMENT_NODE &&
				strcmp((char*)day->name, "dia") == 0)
			n_days++;
	if (!(pred->days = allocate_days(n_days)))
		return;
	pred->n_days = n_days;

	/* Iterate over the days */
	day_index = 0;
	for (xmlNode *day = days; day; day = day->next)
	{
		/* Check if the node is a day */
		if (day->type != XML_ELEMENT_NODE ||
				strcmp((char*)day->name, "dia") != 0)
			continue;

		/* Look for the day data in the attributes list */
		for (day_attr = day->properties;
				day_attr && !pred->days[day_index].data;
				day_attr = day_attr->next)
		{
			if (strcmp((char*)day_attr->name, "fecha") == 0)
				pred->days[day_index].data = strdup((char*)day_attr->children->content);
		}
		parse_day_prediction(pred->days[day_index].hours, day->children);
		day_index++;
	}
}

/*
** This function parses an XML given its root element and returns a prediction
** or NULL in case of error.
*/

static struct prediction_s*
parse_xml(xmlNode *root_element)
{
	struct prediction_s *pred;

	if (!(pred = malloc(sizeof(struct prediction_s))))
	{
		perror("Can't allocate struct prediction_s");
		return (NULL);
	}
	memset(pred, 0, sizeof(struct prediction_s));

	for (xmlNode *root_child = root_element->children;
			root_child; root_child = root_child->next)
	{
		if (root_child->type != XML_ELEMENT_NODE)
			continue;
		else if (strcmp((char*)root_child->name, "nombre") == 0)
			pred->city = strdup((char*)root_child->children->content);
		else if (strcmp((char*)root_child->name, "provincia") == 0)
			pred->province = strdup((char*)root_child->children->content);
		else if (strcmp((char*)root_child->name, "elaborado") == 0)
			parse_updated_data(pred, (char*)root_child->children->content);
		else if (strcmp((char*)root_child->name, "prediccion") == 0)
			parse_prediction(pred, root_child->children);
	}

	return (pred);
}

/*
** Downloads the XML with the prediction for the given city code and returns the
** path to the downloaded file (as a static string). Returns NULL in case of error.
*/

static char*
download_xml(char *city_code)
{
	static char prediction_xml[] = "/tmp/roberto_xml-XXXXXX";
	char url[59];
	CURL *curl;
	int fd;
	FILE *f;

	if (strlen(city_code) == 5)
	{
		sprintf(url, "https://www.aemet.es/xml/municipios_h/localidad_h_%s.xml",
				city_code);
	}
	else
	{
		fprintf(stderr, "Invalid city code: %s\n", city_code);
		return (NULL);
	}

	if ((fd = mkstemp(prediction_xml)) == -1 || (f = fdopen(fd, "w")) == NULL)
	{
		perror("Can't create temporary file at /tmp");
		return (NULL);
	}

	if (!(curl = curl_easy_init()))
	{
		fputs("Can't initialize libCurl\n", stderr);
		return (NULL);
	}
	curl_easy_setopt(curl, CURLOPT_URL, url);
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, fwrite);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, f);
	if (curl_easy_perform(curl) != CURLE_OK) {
		fprintf(stderr, "Something went wrong downloading %s\n", url);
		curl_easy_cleanup(curl);
		fclose(f);
		remove(prediction_xml);
		return (NULL);
	}

	curl_easy_cleanup(curl);
	fclose(f);
	return (prediction_xml);
}

#ifdef LIBXML_TREE_ENABLED

/*
** Given a city code, this function will download and parse the prediction
** for the specified city.
** In case of error, this function returns NULL.
*/

struct prediction_s*
get_prediction(char *city_code)
{
	char *prediction_xml;
	xmlDoc *doc;
	xmlNode *root_element;
	struct prediction_s *prediction;

	LIBXML_TEST_VERSION;
	if (!(prediction_xml = download_xml(city_code)))
		return (NULL);
	if (!(doc = xmlReadFile(prediction_xml, "ISO-8859-15", 0)))
	{
		fprintf(stderr, "Error parsing XML prediction\n");
		return (NULL);
	}
	root_element = xmlDocGetRootElement(doc);

	prediction = parse_xml(root_element);

    xmlFreeDoc(doc);
    xmlCleanupParser();
	remove(prediction_xml);
	return (prediction);
}

#else

/*
** If the libxml in use has been compiled without tree support,
** parse_prediction will display an error and return NULL.
*/

struct prediction_s*
get_prediction(char *city_code)
{
	(void)city_code;
	fprintf(stderr, "Tree support not compiled in libxml\n");
	return (NULL);
}

#endif
