/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ARGUMENTS_PARSER_H
#define ARGUMENTS_PARSER_H

struct args_s
{
	char *city_code;
	int display_n_days;
	int disable_pager;
	int enable_hour;
	int enable_sky_state;
	int enable_precipitation;
	int enable_snow;
	int enable_temperature;
	int enable_termic_sensation;
	int enable_rel_humidity;
	int enable_wind_direction;
	int enable_wind_speed;
	int enable_max_wind_gust;
};

struct args_s*
parse_arguments(int argc, char *argv[]);

void
delete_arguments(struct args_s *args);

#endif
