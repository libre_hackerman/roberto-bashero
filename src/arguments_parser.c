/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <getopt.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <arguments_parser.h>

/*
** Display help text.
*/

static void
display_help(const char *prog_name)
{
	int opt_width = 33;

	printf("Usage: %s [ -c CITY-CODE ] [ -d DAYS ] [ -f FIELD... ] [-h]\n\n",
			prog_name);
	puts("Display weather forecast with data provided by AEMET\n");
	puts("Options:");
	printf("%-*sSet city code (mandatory)\n",
			opt_width, "-c, --city-code <city code>");
	printf("%-*sSet how many days display (Def: 1)\n",
			opt_width, "-d, --days <days>");
	printf("%-*sDisable piping output to a pager\n",
			opt_width, "-n, --no-pager");
	printf("%-*sSet the fields to display on the tables (Def: all)\n\n",
			opt_width, "-f, --fields <field1,field2,...>");
	puts("Fields:");
	printf("hour, sky_state, precipitation, snow, temperature, rel_humidity, ");
	puts("wind_direction, wind_speed, max_wind_gust");
}

/*
** This function allocates and initializes a struct arg_s. Returns NULL in case
** of error.
*/

static struct args_s*
init_arguments()
{
	struct args_s *args;

	if (!(args = malloc(sizeof(struct args_s))))
	{
		perror("Can't allocate struct args_s");
		return (NULL);
	}

	args->city_code = NULL;
	args->display_n_days = 1;
	args->enable_hour = 1;
	args->disable_pager = 0;
	args->enable_sky_state = 1;
	args->enable_precipitation = 1;
	args->enable_snow = 1;
	args->enable_temperature = 1;
	args->enable_termic_sensation = 1;
	args->enable_rel_humidity = 1;
	args->enable_wind_direction = 1;
	args->enable_wind_speed = 1;
	args->enable_max_wind_gust = 1;

	return (args);
}

/*
** This function checks that the city code has the right lenght and only
** contains numbers. Returns 1 if it's valid and 0 if isn't.
*/

static int
check_city_code(const char *city_code)
{
	/* Check lenght */
	if (strlen(city_code) != 5)
		return (0);

	/* Check that only contains digits */
	while (*city_code)
	{
		if (!isdigit(*city_code))
			return (0);
		city_code++;
	}
	return (1);
}

/*
** This function will enable only the fields especified (separated by commas).
** Returns 1 if there were no errors, 0 otherwise.
*/

static int
set_fields(struct args_s *args, const char *fields)
{
	size_t cur_len;
	int end_reached;

	/* Disable all fields */
	args->enable_hour = 0;
	args->enable_sky_state = 0;
	args->enable_precipitation = 0;
	args->enable_snow = 0;
	args->enable_temperature = 0;
	args->enable_termic_sensation = 0;
	args->enable_rel_humidity = 0;
	args->enable_wind_direction = 0;
	args->enable_wind_speed = 0;
	args->enable_max_wind_gust = 0;

	end_reached = 0;
	while (!end_reached)
	{
		/* Get lenght until next comma or to the end of the string */
		if (strchr(fields, ','))
			cur_len = strchr(fields, ',') - fields;
		else
		{
			cur_len = strlen(fields);
			end_reached = 1;
		}

		if (strncmp(fields, "hour", cur_len) == 0)
			args->enable_hour = 1;
		else if (strncmp(fields, "sky_state", cur_len) == 0)
			args->enable_sky_state = 1;
		else if (strncmp(fields, "precipitation", cur_len) == 0)
			args->enable_precipitation = 1;
		else if (strncmp(fields, "snow", cur_len) == 0)
			args->enable_snow = 1;
		else if (strncmp(fields, "temperature", cur_len) == 0)
			args->enable_temperature = 1;
		else if (strncmp(fields, "rel_humidity", cur_len) == 0)
			args->enable_rel_humidity = 1;
		else if (strncmp(fields, "wind_direction", cur_len) == 0)
			args->enable_wind_direction = 1;
		else if (strncmp(fields, "wind_speed", cur_len) == 0)
			args->enable_wind_speed = 1;
		else if (strncmp(fields, "max_wind_gust", cur_len) == 0)
			args->enable_max_wind_gust = 1;
		else
			return (0);

		fields += cur_len + 1;
	}

	return (1);
}

struct args_s*
parse_arguments(int argc, char *argv[])
{
	struct args_s *args;
	int op;

	struct option long_options[] = {
		{"city-code", required_argument, NULL, 'c'},
		{"days", required_argument, NULL, 'd'},
		{"no-pager", no_argument, NULL, 'n'},
		{"fields", required_argument, NULL, 'f'},
		{"help", no_argument, NULL, 'h'},
	};

	if (!(args = init_arguments()))
		return (NULL);

	while ((op = getopt_long(argc, argv, ":c:d:f:nh", long_options, NULL)) != -1)
	{
		switch (op)
		{
			case 'c':
				if (check_city_code(optarg))
					args->city_code = strdup(optarg);
				else
				{
					fprintf(stderr, "Invalid city code: %s\n", optarg);
					delete_arguments(args);
					return (NULL);
				}
				break;
			case 'd':
				if (isdigit(*optarg))
					args->display_n_days = atoi(optarg);
				else
				{
					fprintf(stderr, "Invalid amount of days: %s\n", optarg);
					delete_arguments(args);
					return (NULL);
				}
				break;
			case 'n':
				args->disable_pager = 1;
				break;
			case 'f':
				if (set_fields(args, optarg) == 0)
				{
					fprintf(stderr, "Invalid field: %s\n", optarg);
					delete_arguments(args);
					return (NULL);
				}
				break;
			case 'h':
				display_help(argv[0]);
				delete_arguments(args);
				exit(EXIT_SUCCESS);
			case ':':
				fprintf(stderr, "Missing required argument\n");
				delete_arguments(args);
				return (NULL);
			case '?':
				fprintf(stderr, "Unknown argument, consult %s --help\n", *argv);
				delete_arguments(args);
				return (NULL);
		}
	}

	/* Check for city code */
	if (!args->city_code)
	{
		fprintf(stderr, "City code not specified, consult %s --help\n", *argv);
		delete_arguments(args);
		return (NULL);
	}

	return (args);
}

/*
** This function dealloctes the memory of a struct args_s and its fields.
*/

void
delete_arguments(struct args_s *args)
{
	if (args)
	{
		free(args->city_code);
		free(args);
	}
}
