/*
 * Copyright (C) 2022 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <arguments_parser.h>
#include <prediction_parser.h>
#include <display_prediction.h>
#include <stdlib.h>
#include <stdio.h>
#include <locale.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/ioctl.h>

/*
** Spawns a pager (by default PAGER environment variable, 'less' if unset)
** and pipes stdout to it.
*/

static void
open_pager()
{
	int pipefd[2];
	pid_t pid;
	char *pager;

	if (pipe(pipefd) != 0)
	{
		perror("pipe");
		return;
	}

	if ((pid = fork()) == -1)
	{
		perror("fork");
		return;
	}
	else if (pid == 0)  /* Child */
	{
		close(pipefd[1]);
		dup2(pipefd[0], STDIN_FILENO);

		if ((pager = getenv("PAGER")))
			execl(pager, pager, NULL);
		else
			execlp("less", "less", "-R", NULL);
		perror("execl(p)");
		_exit(EXIT_FAILURE);
	}
	else  /* Parent */
	{
		close(pipefd[0]);
		dup2(pipefd[1], STDOUT_FILENO);
		close(pipefd[1]);
	}
}

/*
** stdout needs to be closed so that the pager receives an EOF. After that,
** it will wait for the pager to exit.
** If no pager was spawned this function will be innocous as long as it's
** called at the end of the program.
*/

static void
close_stdout()
{
	fflush(stdout);
	close(STDOUT_FILENO);
	wait(NULL);
}

/*
** Returns the number of lines of the terminal window that's running the
** program.
*/

static int
get_terminal_lines(void)
{
	struct winsize w;

	if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &w) != 0)
	{
		perror("ioctl TIOCGWINSZ");
		return (0);
	}
	return (w.ws_row);
}

int
main (int argc, char *argv[])
{
	struct args_s *args;
	struct prediction_s *pred;

	if (!(args = parse_arguments(argc, argv)))
		return (EXIT_FAILURE);

	if (!(pred = get_prediction(args->city_code)))
	{
		delete_arguments(args);
		return (EXIT_FAILURE);
	}

	if (isatty(STDOUT_FILENO) && !args->disable_pager &&
			get_terminal_lines() < get_output_linecount(pred, args->display_n_days))
		open_pager();
	setlocale(LC_CTYPE, "");
	display_prediction(pred, args);

	delete_arguments(args);
	delete_prediction(pred);

	close_stdout();
	return (EXIT_SUCCESS);
}
