/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SKY_STATES_H
#define SKY_STATES_H

/*
** This array contains the descriptions in English for the sky state codes
** (represented here by the indexes) in the AEMET XML.
*/

char *sky_states[] = {
	[11] = "clear sky",
	[12] = "slightly cloudy",
	[13] = "cloudy intervals",
	[14] = "cloudy",
	[15] = "very cloudy",
	[16] = "overcast",
	[17] = "hight clouds",
	[43] = "cloudy intervals with little rain",
	[44] = "cloudy with little rain",
	[45] = "very cloudy with little rain",
	[46] = "overcast with little rain",
	[23] = "cloudy intervals with rain",
	[24] = "cloudy with rain",
	[25] = "very cloudy with rain",
	[26] = "overcast with rain",
	[71] = "cloudy intervals with little snow",
	[72] = "cloudy with little snow",
	[73] = "very cloudy with little snow",
	[74] = "overcast with little snow",
	[33] = "cloudy intervals with snow",
	[34] = "cloudy with snow",
	[35] = "very cloudy with snow",
	[36] = "overcast with snow",
	[51] = "cloudy intervals with storm",
	[52] = "cloudy with storm",
	[53] = "very cloudy with storm",
	[54] = "overcast with storm",
	[61] = "cloudy intervals with storm and little rain",
	[62] = "cloudy with storm and little rain",
	[63] = "very cloudy with storm and little rain",
	[64] = "overcast with storm and little rain",
	[81] = "fog",
	[82] = "mist",
	[83] = "haze",
};

#endif
