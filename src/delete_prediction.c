/*
 * Copyright (C) 2021 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <prediction_parser.h>

/*
** Deallocates the memory of the fields of a struct period_s, but not
** the struct itself.
*/

static void
delete_period_data(struct period_s *period)
{
	if (period)
	{
		free(period->precipitation);
		free(period->snow);
		free(period->temperature);
		free(period->termic_sensation);
		free(period->rel_humidity);
		free(period->wind_direction);
		free(period->wind_speed);
		free(period->max_wind_gust);
	}
}

/*
** Deallocates the memory of a prediction
*/

void
delete_prediction(struct prediction_s *pred)
{
	if (pred)
	{
		free(pred->city);
		free(pred->province);
		free(pred->updated_data);

		if (pred->days)
		{
			for (int day = 0; day < pred->n_days; day++)
			{
				free(pred->days[day].data);
				for (int hour = 0; hour <= 23; hour++)
					delete_period_data(&pred->days[day].hours[hour]);
			}
			free(pred->days);
		}
		free(pred);
	}
}
