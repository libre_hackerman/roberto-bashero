/*
 * Copyright (C) 2022 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _XOPEN_SOURCE /* For strptime */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#include <time.h>
#include <prediction_parser.h>
#include <arguments_parser.h>
#include <character_codes.h>
#include <sky_states.h>
#include <display_prediction.h>

#define HOUR_LABEL "Hour"
#define SKY_STATE_LABEL "Sky state"
#define PRECIPITATION_LABEL "Precip. (mm)"
#define SNOW_LABEL "Snow (mm)"
#define TEMPERATURE_LABEL L"Temp. (ºC)"
#define TERMIC_SENSATION_LABEL L"Termic sen. (ºC)"
#define REL_HUMIDITY_LABEL "Rel. Humid. (%)"
#define WIND_DIRECTION_LABEL "Wind dir."
#define WIND_SPEED_LABEL "Wind speed (km/h)"
#define MAX_WIND_GUST_LABEL "Max wind gust (km/h)"

#define LINES_PRED_INFO 5
#define LINES_TABLE_HEADER 5

/*
** Returns non 0 if the passed day is the current day.
*/

static int
is_day_today(struct day_s *day)
{
	time_t now;
	struct tm *now_tm;
	char d[3], m[3], y[5];

	now = time(NULL);
	now_tm = localtime(&now);
	sscanf(day->data, "%4s-%2s-%2s", y, m, d);

	return (atoi(d) == now_tm->tm_mday && atoi(m) == (now_tm->tm_mon + 1) &&
			atoi(y) == (now_tm->tm_year + 1900));
}

/*
** Returns non 0 if the passed day is a past day.
*/

static int
is_day_past(struct day_s *day)
{
	time_t now;
	struct tm day_tm;

	if (is_day_today(day))
		return (0);
	else
	{
		/* If itsn't today, compare the seconds since Epoch */
		memset(&day_tm, 0, sizeof(day_tm));
		strptime(day->data, "%Y-%m-%d", &day_tm);

		now = time(NULL);
		return (now > mktime(&day_tm));
	}
}

/*
** This function calculates the length of the biggest sky state text for the
** day.
*/

static size_t
get_max_len_sky_state(struct day_s *day)
{
	size_t max;
	int sky_state_code;
	int is_today;
	time_t now;
	int hour_now;

	is_today = is_day_today(day);
	now = time(NULL);
	hour_now = localtime(&now)->tm_hour;

	max = strlen(SKY_STATE_LABEL);
	for (int hour = 0; hour <= 23; hour++)
	{
		if (day->hours[hour].initialized == 0 || (is_today && hour < hour_now))
			continue;

		sky_state_code = day->hours[hour].sky_state;
		if (strlen(sky_states[sky_state_code]) > max)
			max = strlen(sky_states[sky_state_code]);
	}

	return (max);
}

/*
** This function draws an horizontal separator given a day, the argument
** options and the desired characters. Returns the width of the separator.
*/

static size_t
draw_horizontal_separator(struct day_s *day, struct args_s *args,
		wchar_t left_ch, wchar_t horiz_ch, wchar_t inter_ch, wchar_t right_ch)
{
	size_t width, field_width;

	width = 1;
	putwchar(left_ch);

	/* Hour field */
	if (args->enable_hour)
	{
		field_width = strlen(HOUR_LABEL) + 3;
		width += field_width;
		for (size_t i = 0; i < field_width - 1; i++)
			putwchar(horiz_ch);

		/* Intersection character */
		if (args->enable_sky_state || args->enable_precipitation ||
				args->enable_snow || args->enable_temperature ||
				args->enable_termic_sensation || args->enable_rel_humidity ||
				args->enable_wind_direction || args->enable_wind_speed ||
				args->enable_max_wind_gust)
			putwchar(inter_ch);
	}

	/* Sky state field */
	if (args->enable_sky_state)
	{
		field_width = get_max_len_sky_state(day) + 3;
		width += field_width;
		for (size_t i = 0; i < field_width - 1; i++)
			putwchar(horiz_ch);

		/* Intersection character */
		if (args->enable_precipitation || args->enable_max_wind_gust ||
				args->enable_snow || args->enable_temperature ||
				args->enable_termic_sensation || args->enable_rel_humidity ||
				args->enable_wind_direction || args->enable_wind_speed)
			putwchar(inter_ch);
	}

	/* Precipitation field */
	if (args->enable_precipitation)
	{
		field_width = strlen(PRECIPITATION_LABEL) + 3;
		width += field_width;
		for (size_t i = 0; i < field_width - 1; i++)
			putwchar(horiz_ch);

		/* Intersection character */
		if (args->enable_max_wind_gust || args->enable_wind_speed ||
				args->enable_snow || args->enable_temperature ||
				args->enable_termic_sensation || args->enable_rel_humidity ||
				args->enable_wind_direction)
			putwchar(inter_ch);
	}

	/* Snow field */
	if (args->enable_snow)
	{
		field_width = strlen(SNOW_LABEL) + 3;
		width += field_width;
		for (size_t i = 0; i < field_width - 1; i++)
			putwchar(horiz_ch);

		/* Intersection character */
		if (args->enable_max_wind_gust || args->enable_wind_speed ||
				args->enable_temperature || args->enable_wind_direction ||
				args->enable_termic_sensation || args->enable_rel_humidity)
			putwchar(inter_ch);
	}

	/* Temperature field */
	if (args->enable_temperature)
	{
		field_width = wcslen(TEMPERATURE_LABEL) + 3;
		width += field_width;
		for (size_t i = 0; i < field_width - 1; i++)
			putwchar(horiz_ch);

		/* Intersection character */
		if (args->enable_max_wind_gust || args->enable_wind_speed ||
				args->enable_temperature || args->enable_wind_direction ||
				args->enable_termic_sensation || args->enable_rel_humidity)
			putwchar(inter_ch);
	}

	/* Termic_sensation field */
	if (args->enable_termic_sensation)
	{
		field_width = wcslen(TERMIC_SENSATION_LABEL) + 3;
		width += field_width;
		for (size_t i = 0; i < field_width - 1; i++)
			putwchar(horiz_ch);

		/* Intersection character */
		if (args->enable_max_wind_gust || args->enable_wind_speed ||
				args->enable_wind_direction || args->enable_rel_humidity)
			putwchar(inter_ch);
	}

	/* Rel_humidity field */
	if (args->enable_rel_humidity)
	{
		field_width = strlen(REL_HUMIDITY_LABEL) + 3;
		width += field_width;
		for (size_t i = 0; i < field_width - 1; i++)
			putwchar(horiz_ch);

		/* Intersection character */
		if (args->enable_max_wind_gust || args->enable_wind_speed ||
				args->enable_wind_direction)
			putwchar(inter_ch);
	}

	/* Wind_direction field */
	if (args->enable_wind_direction)
	{
		field_width = strlen(WIND_DIRECTION_LABEL) + 3;
		width += field_width;
		for (size_t i = 0; i < field_width - 1; i++)
			putwchar(horiz_ch);

		/* Intersection character */
		if (args->enable_max_wind_gust || args->enable_wind_speed)
			putwchar(inter_ch);
	}

	/* Wind_speed field */
	if (args->enable_wind_speed)
	{
		field_width = strlen(WIND_SPEED_LABEL) + 3;
		width += field_width;
		for (size_t i = 0; i < field_width - 1; i++)
			putwchar(horiz_ch);

		/* Intersection character */
		if (args->enable_max_wind_gust)
			putwchar(inter_ch);
	}

	/* Max_wind_gust field */
	if (args->enable_max_wind_gust)
	{
		field_width = strlen(MAX_WIND_GUST_LABEL) + 3;
		width += field_width;
		for (size_t i = 0; i < field_width - 1; i++)
			putwchar(horiz_ch);
	}
	wprintf(L"%lc\n", right_ch);

	return (width);
}

/*
** This function displays the line with the field names for the table header.
*/

static void
display_field_names(struct day_s *day, struct args_s *args)
{
	putwchar(DBOX_VERTICAL);
	if (args->enable_hour)
		wprintf(L" %s %lc", HOUR_LABEL, DBOX_VERTICAL);
	if (args->enable_sky_state)
	{
		wprintf(L" %-*s %lc", get_max_len_sky_state(day),
				SKY_STATE_LABEL, DBOX_VERTICAL);
	}
	if (args->enable_precipitation)
		wprintf(L" %s %lc", PRECIPITATION_LABEL, DBOX_VERTICAL);
	if (args->enable_snow)
		wprintf(L" %s %lc", SNOW_LABEL, DBOX_VERTICAL);
	if (args->enable_temperature)
		wprintf(L" %ls %lc", TEMPERATURE_LABEL, DBOX_VERTICAL);
	if (args->enable_termic_sensation)
		wprintf(L" %ls %lc", TERMIC_SENSATION_LABEL, DBOX_VERTICAL);
	if (args->enable_rel_humidity)
		wprintf(L" %s %lc", REL_HUMIDITY_LABEL, DBOX_VERTICAL);
	if (args->enable_wind_direction)
		wprintf(L" %s %lc", WIND_DIRECTION_LABEL, DBOX_VERTICAL);
	if (args->enable_wind_speed)
		wprintf(L" %s %lc", WIND_SPEED_LABEL, DBOX_VERTICAL);
	if (args->enable_max_wind_gust)
		wprintf(L" %s %lc", MAX_WIND_GUST_LABEL, DBOX_VERTICAL);
	putwchar('\n');
}

/*
** Yeah Sherlock, this function displays the table header.
*/

static void
display_header(struct day_s *day, struct args_s *args)
{
	size_t width;
	size_t chars_left;
	char d[3], m[3], y[5];

	/* Top line */
	width = draw_horizontal_separator(day, args, DBOX_UPPER_LEFT_CORNER, 
			DBOX_HORIZONTAL, DBOX_HORIZONTAL, DBOX_UPPER_RIGHT_CORNER);

	/* Day data */
	sscanf(day->data, "%4s-%2s-%2s", y, m, d);
	chars_left = width - wprintf(L"%lc Day: %s/%s/%s", DBOX_VERTICAL, d, m, y);
	for (size_t i = 0; i < chars_left - 1; i++)
		putwchar(' ');
	wprintf(L"%lc\n", DBOX_VERTICAL);

	/* Middle line */
	draw_horizontal_separator(day, args, DBOX_VERTICAL_RIGHT, DBOX_HORIZONTAL,
			DBOX_HORIZONTAL_DOWN, DBOX_VERTICAL_LEFT);

	/* Field names */
	display_field_names(day, args);

	/* Bottom line */
	draw_horizontal_separator(day, args, DBOX_VERTICAL_RIGHT, DBOX_HORIZONTAL,
			DBOX_CROSS, DBOX_VERTICAL_LEFT);
}

/*
** This function displays a temperature cell.
*/

static void
display_temperature(const char *temperature, const wchar_t *label)
{
	int temp;

	temp = atoi(temperature);
	if (temp <= 15)
		wprintf(L"%s", FOR_CYAN);
	else if (temp >= 30)
		wprintf(L"%s", FOR_RED);
	wprintf(L" %-*s%s ", wcslen(label), temperature, RESET);
}

/*
** This function displays a sky state cell.
*/

static void
display_sky_state(int sky_code, size_t sky_state_len)
{
	/* More or less clear skies */
	if ((sky_code >= 11 && sky_code <= 13) || sky_code == 17)
		wprintf(L"%s", FOR_YELLOW);
	/* Rain */
	else if (sky_code >= 23 && sky_code <= 26)
		wprintf(L"%s", FOR_BLUE);
	/* Snow */
	else if ((sky_code >= 33 && sky_code <= 36) ||
			(sky_code >= 71 && sky_code <= 74))
		wprintf(L"%s", BOLD);
	/* Light rain */
	else if (sky_code >= 43 && sky_code <= 46)
		wprintf(L"%s", FOR_CYAN);
	/* Storm */
	else if (sky_code >= 51 && sky_code <= 64)
		wprintf(L"%s", FOR_RED);
	/* Atmosphere particles */
	else if (sky_code >= 81 && sky_code <= 83)
		wprintf(L"%s", FOR_PURPLE);

	wprintf(L" %-*s%s ", sky_state_len, sky_states[sky_code], RESET);
}

/*
** This function displays a wind direction cell.
*/

static void
display_wind_direction(const char *wind_dir)
{
	wchar_t arrow;

	/* Select arrow */
	if (strcmp(wind_dir, "N") == 0)
		arrow = DOWN_ARROW;
	else if (strcmp(wind_dir, "NE") == 0)
		arrow = DOWN_LEFT_ARROW;
	else if (strcmp(wind_dir, "E") == 0)
		arrow = LEFT_ARROW;
	else if (strcmp(wind_dir, "SE") == 0)
		arrow = UP_LEFT_ARROW;
	else if (strcmp(wind_dir, "S") == 0)
		arrow = UP_ARROW;
	else if (strcmp(wind_dir, "SO") == 0)
		arrow = UP_RIGHT_ARROW;
	else if (strcmp(wind_dir, "O") == 0)
		arrow = RIGHT_ARROW;
	else if (strcmp(wind_dir, "NO") == 0)
		arrow = DOWN_RIGHT_ARROW;
	else if (strcmp(wind_dir, "C") == 0)
		arrow = L'-';
	else
		arrow = L'?';

	/* Display direction letters */
	if (strcmp(wind_dir, "C") == 0)
		wprintf(L"  ");
	else
	{
		for (int i = 0; wind_dir[i]; i++)
		{
			if (wind_dir[i] == 'N')
				wprintf(L" %s%c", FOR_RED, 'N');
			if (wind_dir[i] == 'S')
				wprintf(L" %s%c", FOR_BLUE, 'S');
			if (wind_dir[i] == 'E')
				wprintf(L" %s%c", FOR_GREEN, 'E');
			if (wind_dir[i] == 'O')
				wprintf(L" %s%c", FOR_PURPLE, 'W');
		}
	}
	wprintf(L"%s %lc %*s ", RESET, arrow, strlen(WIND_DIRECTION_LABEL) - 3 -
			(strlen(wind_dir) == 1 ? 1 : 3), " ");
}

/*
** This function displays a row for a given hour.
*/

static void
display_table_row(struct period_s *hour, int hour_n, struct args_s *args,
		size_t sky_state_len)
{
	putwchar(DBOX_VERTICAL);
	/* Hour field */
	if (args->enable_hour)
	{
		wprintf(L" %-*.2d ", strlen(HOUR_LABEL), hour_n);

		/* Vertical bar in between fields */
		if (args->enable_sky_state || args->enable_precipitation ||
				args->enable_snow || args->enable_temperature ||
				args->enable_termic_sensation || args->enable_rel_humidity ||
				args->enable_wind_direction || args->enable_wind_speed ||
				args->enable_max_wind_gust)
			putwchar(BOX_VERTICAL);
	}

	/* Sky state field */
	if (args->enable_sky_state)
	{
		display_sky_state(hour->sky_state, sky_state_len);

		/* Vertical bar in between fields */
		if (args->enable_precipitation || args->enable_max_wind_gust ||
				args->enable_snow || args->enable_temperature ||
				args->enable_termic_sensation || args->enable_rel_humidity ||
				args->enable_wind_direction || args->enable_wind_speed)
			putwchar(BOX_VERTICAL);
	}

	/* Precipitation field */
	if (args->enable_precipitation)
	{
		if (strcmp(hour->precipitation, "0") != 0)
			wprintf(L"%s", FOR_BLUE);
		wprintf(L" %-*s%s ", strlen(PRECIPITATION_LABEL), hour->precipitation,
				RESET);

		/* Vertical bar in between fields */
		if (args->enable_max_wind_gust || args->enable_wind_speed ||
				args->enable_snow || args->enable_temperature ||
				args->enable_termic_sensation || args->enable_rel_humidity ||
				args->enable_wind_direction)
			putwchar(BOX_VERTICAL);
	}

	/* Snow field */
	if (args->enable_snow)
	{
		if (strcmp(hour->snow, "0") != 0)
			wprintf(L"%s", FOR_CYAN);
		wprintf(L" %-*s%s ", strlen(SNOW_LABEL), hour->snow, RESET);

		/* Vertical bar in between fields */
		if (args->enable_max_wind_gust || args->enable_wind_speed ||
				args->enable_temperature || args->enable_wind_direction ||
				args->enable_termic_sensation || args->enable_rel_humidity)
			putwchar(BOX_VERTICAL);
	}

	/* Temperature field */
	if (args->enable_temperature)
	{
		display_temperature(hour->temperature, TEMPERATURE_LABEL);

		/* Vertical bar in between fields */
		if (args->enable_max_wind_gust || args->enable_wind_speed ||
				args->enable_wind_direction || args->enable_rel_humidity ||
				args->enable_termic_sensation)
			putwchar(BOX_VERTICAL);
	}

	/* Termic sensation field */
	if (args->enable_termic_sensation)
	{
		display_temperature(hour->temperature, TERMIC_SENSATION_LABEL);

		/* Vertical bar in between fields */
		if (args->enable_max_wind_gust || args->enable_wind_speed ||
				args->enable_wind_direction || args->enable_rel_humidity)
			putwchar(BOX_VERTICAL);
	}

	/* Relative humidity field */
	if (args->enable_rel_humidity)
	{
		if (atoi(hour->rel_humidity) >= 80)
			wprintf(L"%s", FOR_BLUE);
		wprintf(L" %-*s%s ", strlen(REL_HUMIDITY_LABEL), hour->rel_humidity,
				RESET);

		/* Vertical bar in between fields */
		if (args->enable_max_wind_gust || args->enable_wind_speed ||
				args->enable_wind_direction)
			putwchar(BOX_VERTICAL);
	}

	/* Wind direction field */
	if (args->enable_wind_direction)
	{
		display_wind_direction(hour->wind_direction);

		/* Vertical bar in between fields */
		if (args->enable_max_wind_gust || args->enable_wind_speed)
			putwchar(BOX_VERTICAL);
	}

	/* Wind speed field */
	if (args->enable_wind_speed)
	{
		if (atoi(hour->wind_speed) >= 35)
			wprintf(L"%s", FOR_YELLOW);
		wprintf(L" %-*s%s ", strlen(WIND_SPEED_LABEL), hour->wind_speed,
				RESET);

		/* Vertical bar in between fields */
		if (args->enable_max_wind_gust)
			putwchar(BOX_VERTICAL);
	}

	if (args->enable_max_wind_gust)
	{
		if (atoi(hour->max_wind_gust) >= 40)
			wprintf(L"%s", FOR_RED);
		wprintf(L" %-*s%s ", strlen(MAX_WIND_GUST_LABEL), hour->max_wind_gust,
				RESET);
	}

	wprintf(L"%lc\n", DBOX_VERTICAL);
}

/*
** Display the table with the prediction for a particular day.
*/

static void
display_table(struct day_s *day, struct args_s *args)
{
	size_t sky_state_len;
	time_t now;
	int is_today;
	int hour_now;

	is_today = is_day_today(day);
	now = time(NULL);
	hour_now = localtime(&now)->tm_hour;

	display_header(day, args);

	sky_state_len = get_max_len_sky_state(day);
	for (int hour = 0; hour <= 23; hour++)
	{
		if (day->hours[hour].initialized && !(is_today && hour < hour_now))
		{
			display_table_row(&day->hours[hour], hour, args, sky_state_len);

			/* Thin separator */
			if (hour < 23 && day->hours[hour + 1].initialized)
			{
				draw_horizontal_separator(day, args, DBOX_VERTICAL_S_RIGHT,
						BOX_HORIZONTAL, BOX_CROSS, DBOX_VERTICAL_S_LEFT);
			}
		}
	}

	/* Bottom table separator */
	draw_horizontal_separator(day, args, DBOX_DOWN_LEFT_CORNER, DBOX_HORIZONTAL,
			DBOX_HORIZONTAL_S_UP, DBOX_DOWN_RIGHT_CORNER);
}

/*
** Returns the how many hours would be displayed for the given day.
*/

static int
get_displayable_hours(struct day_s *day)
{
	int count;
	time_t now;
	int is_today;
	int hour_now;

	is_today = is_day_today(day);
	now = time(NULL);
	hour_now = localtime(&now)->tm_hour;

	count = 0;
	for (int i = 0; i <= 23; i++)
		if (day->hours[i].initialized && (!is_today || i >= hour_now))
			count++;
	return (count);
}

/*
** Returns the number of lines the output of the program should write
** for a given prediction.
*/

int
get_output_linecount(struct prediction_s *pred, int display_n_days)
{
	int count;

	count = LINES_PRED_INFO;
	for (int day = 0, days_offset = 0;
			day < pred->n_days && day - days_offset < display_n_days;
			day++)
	{
		if (is_day_past(&pred->days[day]))
			days_offset++;
		else
		{
			count += LINES_TABLE_HEADER;
			count += get_displayable_hours(&pred->days[day]) * 2;
		}
	}
	return count;
}

/*
** This function displays the prediction tables as set in the arguments.
*/

void
display_prediction(struct prediction_s *pred, struct args_s *args)
{
	/* Display location */
	wprintf(L"Prediction for %s%s%s, %s%s\n\n", BOLD, FOR_RED, pred->city,
			pred->province, RESET);

	/* Display last updated data */
	wprintf(L"Updated: %s%s%s\n\n", BOLD, pred->updated_data, RESET);

	/* Display a table with the prediction for each day */
	for (int day = 0, days_offset = 0;
			day < pred->n_days && day - days_offset < args->display_n_days;
			day++)
	{
		if (is_day_past(&pred->days[day]))
			days_offset++;
		else
			display_table(&pred->days[day], args);
	}

	/* AEMET note */
	wprintf(L"%sInformation elaborated by %s%sAEMET%s\n", UNDERLINE, BOLD,
			FOR_CYAN, RESET);
}
